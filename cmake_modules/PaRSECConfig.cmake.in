set(PARSEC_VERSION @PARSEC_VERSION_MAJOR@.@PARSEC_VERSION_MINOR@.@PARSEC_VERSION_PATCH@)

@PACKAGE_INIT@

set_and_check(PARSEC_DIR "${PACKAGE_PREFIX_DIR}")
set_and_check(PARSEC_INCLUDE_DIRS "@PACKAGE_INCLUDE_INSTALL_DIR@")
set_and_check(PARSEC_SYSCONFIG_DIRS "@PACKAGE_SYSCONFIG_INSTALL_DIR@")
set_and_check(PARSEC_CONFIG_DIRS "@PACKAGE_CONFIG_INSTALL_DIR@")
set_and_check(PARSEC_LIBRARY_DIRS "@PACKAGE_LIB_INSTALL_DIR@")
set_and_check(PARSEC_BINARY_DIRS "@PACKAGE_RUNTIME_INSTALL_DIR@")

set(PARSEC_PTGFLAGS "$ENV{PTGFLAGS}")

# Pull the dependencies
list(APPEND CMAKE_PREFIX_PATH "${PARSEC_CONFIG_DIRS}")
list(APPEND CMAKE_MODULE_PATH "${PARSEC_CONFIG_DIRS}")

find_package(Threads)

if(@PARSEC_HAVE_HWLOC@)
  # If HWLOC is found on the system directories the HWLOC_DIR is set to ""
  if("@HWLOC_DIR@")
    set_and_check(HWLOC_DIR "@HWLOC_DIR@")
  endif("@HWLOC_DIR@")
  find_package(HWLOC REQUIRED)
endif(@PARSEC_HAVE_HWLOC@)

if(@PARSEC_HAVE_OTF2@)
  # Nothing exportable here, if this test succeed then PaRSEC supports OTF2 output.
endif(@PARSEC_HAVE_OTF2@)

if(@PARSEC_HAVE_PAPI@)
  set_and_check(PAPI_INCLUDE_DIR "@PAPI_INCLUDE_DIR@")
  set_and_check(PAPI_LIBRARY "@PAPI_LIBRARY@")
endif(@PARSEC_HAVE_PAPI@)

if(@PARSEC_DIST_WITH_MPI@)
  include(CheckCSourceCompiles)
  # Test first if we are using a wrapper as the compiler
  # In that case, we don't need to change the include path and
  # dependent libraries.
  CHECK_C_SOURCE_COMPILES("#include <mpi.h>
                           int main(int argc, char *argv[]) {
                             MPI_Init(&argc, &argv);
                             MPI_Barrier(MPI_COMM_WORLD);
                             MPI_Finalize();
                             return 0;
                           }" CC_CONTAINS_MPI)
  if( CC_CONTAINS_MPI )
    # Create fake MPI:: targets
    set(MPI_C_FOUND TRUE)
    add_library(MPI::MPI_C INTERFACE IMPORTED)
    set(MPI_Fortran_FOUND TRUE)
    add_library(MPI::MPI_Fortran INTERFACE IMPORTED)
  else( CC_CONTAINS_MPI )
    # Otherwise try to find MPI in the normal way
    find_package(MPI REQUIRED)
  endif( CC_CONTAINS_MPI )
endif(@PARSEC_DIST_WITH_MPI@)

if(@PARSEC_HAVE_CUDA@)
  find_package(CUDA REQUIRED)
  SET(PARSEC_HAVE_CUDA TRUE)
endif(@PARSEC_HAVE_CUDA@)

if(@PARSEC_PROF_TRACE@)
  # Nothing exportable here, if this test succeed then PaRSEC supports tracing
endif(@PARSEC_PROF_TRACE@)

# Pull the PaRSEC::<targets>
if(NOT TARGET PaRSEC::parsec)
  include(${CMAKE_CURRENT_LIST_DIR}/PaRSECTargets.cmake)
endif(NOT TARGET PaRSEC::parsec)

